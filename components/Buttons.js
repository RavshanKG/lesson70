import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export default class Buttons extends React.Component {

  render() {


    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('1')}}>
            <View>
              <Text style={styles.text}>1</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('4')}}>
            <View>
              <Text style={styles.text}>4</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('7')}}>
            <View>
              <Text style={styles.text}>7</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('2')}}>
            <View>
              <Text style={styles.text}>2</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('5')}}>
            <View>
              <Text style={styles.text}>5</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('8')}}>
            <View>
              <Text style={styles.text}>8</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('3')}}>
            <View>
              <Text style={styles.text}>3</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('6')}}>
            <View>
              <Text style={styles.text}>6</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('9')}}>
            <View>
              <Text style={styles.text}>9</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity style={styles.cell} onPress={this.props.clear}>
            <View>
              <Text style={styles.text}>C</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={this.props.deleteLastDigit}>
            <View>
              <Text style={styles.text}>&#x2190;</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={this.props.ButtonPressed.bind(this, '+')}>
            <View>
              <Text style={styles.text}>+</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={this.props.ButtonPressed.bind(this, '-')}>
            <View>
              <Text style={styles.text}>-</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={this.props.ButtonPressed.bind(this, '*')}>
            <View>
              <Text style={styles.text}>*</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={this.props.ButtonPressed.bind(this, '/')}>
            <View>
              <Text style={styles.text}>/</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={this.props.calculate}>
            <View>
              <Text style={styles.text}>=</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.cell} onPress={()=> {this.props.ButtonPressed('0')}}>
            <View>
              <Text style={styles.text}>0</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    padding: 2,
    flexDirection: 'row',

  },
  row: {
    flex: 1,
    flexDirection: 'column'
  },
  cell: {
    flex: 1,
    backgroundColor: 'rgb(26, 164, 147)',
    borderWidth: 2,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
