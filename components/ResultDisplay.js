import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class ResultDisplay extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <Text>{this.props.digits}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(231, 206, 244)',
    padding: 20,

  },
});
