import React from 'react';
import {StyleSheet, View} from 'react-native';
import Buttons from './components/Buttons';
import ResultDisplay from './components/ResultDisplay';

export default class App extends React.Component {
  state = {
    digits: ''
  };



  ButtonPressed = (value) => {
    let digits = this.state.digits;
    digits += value;
    this.setState({digits});
  };

  calculate = () => {
    let result = eval(this.state.digits);
    this.setState({digits: result})
  };

  clear = () => {
    this.setState({digits: ''})
  };

  deleteLastDigit = () => {
    let digits = this.state.digits;
    let a = digits.slice(0, -1);
    this.setState({digits: a})
  };



  render() {
    return (
      <View style={styles.container}>
        <ResultDisplay digits={this.state.digits}/>
        <Buttons ButtonPressed={this.ButtonPressed}
                 calculate={this.calculate}
                 clear={this.clear}
                 deleteLastDigit={this.deleteLastDigit}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(231, 206, 244)',
  },
});
